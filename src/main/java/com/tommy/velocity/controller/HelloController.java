package com.tommy.velocity.controller;

import com.tommy.velocity.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.Map;

/**
 * @author j.tommy
 * @version 1.0
 * @date 2018/4/28
 */
@Controller
public class HelloController {
    private final static Map<Integer,User> users = new HashMap<>(8);

    static {
        initData();
    }

    @RequestMapping(value = "/list")
    public String showList(ModelMap map) {
        map.put("users",users);
        return "/list";
    }

    @RequestMapping(value = "/preEdit")
    public String preEdit(ModelMap map, Integer userId) {
        User user = users.get(userId);
        map.put("user",user);
        return "/edit";
    }

    @RequestMapping(value = "/edit")
    public String edit(ModelMap map, User user) {
        users.put(user.getId(),user);
        return showList(map);
    }

    private static void initData() {
        for (int i=0;i<8;i++) {
            User user = new User();
            user.setId(i);
            user.setName("测试用户" + i);
            user.setAge((int) (Math.random()*100+10));
            users.put(i, user);
        }
    }
}
